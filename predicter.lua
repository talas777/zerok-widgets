function widget:GetInfo()
	return {
		name         = "Predicter",
		desc         = "Predicts game outcomes before game starts",
		author       = "Brackman",
		date         = "2018-07-11, last change 2022-09-30",
		license      = "GNU GPL v2 or v3 but not later",
		-- layer        = 0,
		enabled      = true
	}
end

local mround = math.round
local mexp = math.exp
local eloBeta = (math.log(10))/400

local updateCounter = 0
local spGetPlayerInfo = Spring.GetPlayerInfo
local spGetTeamList = Spring.GetTeamList
local spGetPlayerList = Spring.GetPlayerList
local spEcho = Spring.Echo
local myPlayerID = Spring.GetMyPlayerID()

function widget:Initialize()
	spEcho("LOADED "..widget:GetInfo().name)
end

function widget:Update()
	if updateCounter >= 90 then
		local whrGameText = {}
		local nrOfAddedAllyTeams = 0
		local teamStrengthSum = 0
		for allyTeamID,allyTeamData in pairs(Spring.GetAllyTeamList()) do
			local allyTeamTeams = spGetTeamList(allyTeamData)
			if allyTeamTeams and next(allyTeamTeams) then
				local whrTeamText = ""
				local alreadyAddedAPlayerOfThisTeam = false
				local whrTeamSum
				local whrTeamSize
				local whrTeamMax
				local whrTeamMaxPlayerID
				for teamID,teamData in pairs(allyTeamTeams) do
					local teamPlayers = spGetPlayerList(teamData)
					if teamPlayers and next(teamPlayers) then
						--spEcho("game_message: Squad "..teamData.." has "..#teamPlayers.." Players")
						for id,playerData in pairs(teamPlayers) do
							local playerWhrString = select(10,spGetPlayerInfo(playerData)).elo
							local playerSpec = select(3,spGetPlayerInfo(playerData))
							if playerWhrString and playerSpec == false then
								local playerWhrNr = tonumber(playerWhrString)
								if alreadyAddedAPlayerOfThisTeam then
									if playerWhrNr then
										whrTeamSum = whrTeamSum + playerWhrNr
										whrTeamSize = whrTeamSize + 1
									end
									if playerData == myPlayerID then
										whrTeamText = playerWhrString..", "..whrTeamText
										whrTeamMax = math.huge
										whrTeamMaxPlayerID = playerData
									elseif (whrTeamMax and whrTeamMax >= playerWhrNr) or not playerWhrNr then
										whrTeamText = whrTeamText..", "..playerWhrString
									else
										whrTeamText = playerWhrString..", "..whrTeamText
										whrTeamMax = playerWhrNr
										whrTeamMaxPlayerID = playerData
									end
								else
									whrTeamText = playerWhrString
									if playerData == myPlayerID then
										whrTeamMax = math.huge
									else
										whrTeamMax = playerWhrNr
									end
									if playerWhrNr then
										whrTeamSum = playerWhrNr
										whrTeamSize = 1
									else
										whrTeamSum = 0
										whrTeamSize = 0
									end
									alreadyAddedAPlayerOfThisTeam = true
									whrTeamMaxPlayerID = playerData
								end
							end
						end
					end
				end
				if alreadyAddedAPlayerOfThisTeam then
					local whrTeamAvg = nil
					if whrTeamSize > 0 then 
						whrTeamAvg = whrTeamSum/whrTeamSize
						teamStrengthSum = teamStrengthSum + mexp(whrTeamAvg * eloBeta)
					end
					nrOfAddedAllyTeams = nrOfAddedAllyTeams + 1
					whrGameText[nrOfAddedAllyTeams] = {whr = whrTeamAvg, txt = (spGetPlayerInfo(whrTeamMaxPlayerID) or allyTeamData).." ("..whrTeamText..")"}
				end
			end
		end
		if teamStrengthSum == 0 then
			teamStrengthSum = 1
		end
		for allyTeamIdx = 1,nrOfAddedAllyTeams do
			local whrGameTextEntry = whrGameText[allyTeamIdx]
			local whr = whrGameTextEntry.whr
			local winPercentage = 0
			if whr then
				winPercentage = mround(100*mexp(whr * eloBeta)/teamStrengthSum)
				whr = mround(whr)
			end
			spEcho("game_message: "..winPercentage.."%: "..(whr or "?").." Team "..whrGameTextEntry.txt)
		end
		spEcho(widget:GetInfo().name.." REMOVES ITSELF")
		widgetHandler:RemoveWidget()
	end
	updateCounter = updateCounter + 1
end
