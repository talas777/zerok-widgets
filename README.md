# ZeroK-Widgets

This is widgets for Zero-K that I use, have used, or have modified.

The main interest is probably my version of Global Build Command.


## Usage

Place the widgets in you LuaUI/Widgets folder.
On steam it lives inside ~/.steam/steam/steamapps/common/Zero-K
You might have to create the Widgets folder.

After installing a widget, it can be enabled in the Widget List ingame.
Some widgets are automatically enabled, but there can be conflicts.

