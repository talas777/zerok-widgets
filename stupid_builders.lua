-- Copyright (C) 2022 Brackman
function widget:GetInfo()
	return {
		name         = "Stupid Builders",
		desc         = "Notifies player and widgets of Smartest Builders crashes and reactivations",
		author       = "Brackman",
		date         = "2022-01-16",
		license      = "GNU GPL v2",
		--layer        = 0,
		enabled      = true
	}
end

local smartestBuildersCrashedLocal = nil
local crashText = nil

local function changeStatus(crash)
	if not crashText then
		crashText = "game_message:"..Spring.GetPlayerInfo(Spring.GetMyPlayerID()).."'s builders are "
	end
	if crash then
		Spring.Echo(crashText.."no longer smart!")
	else
		Spring.Echo(crashText.."smart again!")
	end
	smartestBuildersCrashedLocal = crash
	WG.smartestBuildersCrashed = crash
end

function widget:Initialize()
	local loadText  = "LOADED "
	if(Spring.IsReplay() or Spring.GetSpectatingState()) then
		Spring.Echo(loadText.."AND REMOVED "..widget:GetInfo().name)
		widgetHandler:RemoveWidget()
	else
		Spring.Echo(loadText..widget:GetInfo().name)
		WG.smartestBuildersCrashed = false
	end
end

function widget:GameFrame(frame)
	if frame % 90 == 75 then
		local smartestBuildersFrameLocal = WG.smartestBuildersFrame
		if smartestBuildersFrameLocal and smartestBuildersFrameLocal + 90 > frame then
			if smartestBuildersCrashedLocal then
				changeStatus(false)
			end
		elseif not smartestBuildersCrashedLocal then
			changeStatus(true)
		end
	end
end