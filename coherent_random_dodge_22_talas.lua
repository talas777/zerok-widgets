-- Copyright (C) 2021 Brackman
function widget:GetInfo()
    return {
        name         = "Coherent Random Dodge(talas mod)",
        desc         = "Certain idle selected uncloaked mobile units that are not queued to be transported dodge on a line perpendicular to the line to the next enemy",
        author       = "Brackman",
        date         = "2021-12-19",
        license      = "GNU GPL v2",
        -- layer        = 0,
        enabled      = true
    }
end

include("keysym.h.lua")
VFS.Include("LuaRules/Configs/customcmds.h.lua")

local spGetUnitPosition = Spring.GetUnitPosition
local spGetUnitNearestEnemy = Spring.GetUnitNearestEnemy
local spGiveOrderToUnit = Spring.GiveOrderToUnit
local spGetUnitStates = Spring.GetUnitStates
local spGetUnitHealth = Spring.GetUnitHealth
local myTeamID = Spring.GetMyTeamID()
local spGetUnitCommands = Spring.GetUnitCommands
local spValidUnitID = Spring.ValidUnitID
local spGetUnitDefID = Spring.GetUnitDefID

local CMD_LOAD_UNITS = CMD.LOAD_UNITS
local spIsUnitSelected = Spring.IsUnitSelected
-- 74145: Changed to whitelist instead
local maybeDodgers = {}
-- AA, that might need to dodge nimbus fire
maybeDodgers[ UnitDefNames["cloakaa"].id ] = true -- Gremlin
maybeDodgers[ UnitDefNames["shieldaa"].id ] = true -- Vandal / unsure
-- Riots, that might try to dodge skirms
--maybeDodgers[ UnitDefNames["cloakriot"].id ] = true -- Reaver / eh, not very helpful? disabled for now..
-- Raiders, that could maybe survive a bit longer by dodging around
maybeDodgers[ UnitDefNames["jumpraid"].id ] = true -- Pyro / unsure
maybeDodgers[ UnitDefNames["amphraid"].id ] = true -- Duck / unsure
-- Skirms that might need to dodge other skirms / arty
maybeDodgers[ UnitDefNames["shipskirm"].id ] = true -- Mistral / unsure
maybeDodgers[ UnitDefNames["cloakskirm"].id ] = true -- Ronin / unsure
maybeDodgers[ UnitDefNames["shieldskirm"].id ] = true -- Rogue / unsure
maybeDodgers[ UnitDefNames["spiderskirm"].id ] = true -- Recluse / unsure
maybeDodgers[ UnitDefNames["jumpskirm"].id ] = true -- Moderator / unsure
--local fencerID = UnitDefNames["vehsupport"].id
--local crabID = UnitDefNames["spidercrabe"].id
--local NimbusID = UnitDefNames["gunshipheavyskirm"].id
--local BulkheadID = UnitDefNames["amphsupport"].id
--local EmissaryID = UnitDefNames["tankarty"].id
--local TremorID = UnitDefNames["tankheavyarty"].id
local CharonID = UnitDefNames["gunshiptrans"].id
local HerculesID = UnitDefNames["gunshipheavytrans"].id

local dodgerIDs = {}
local dodgerPositions = {}
local dodgerNr = 0
local updateCounter = 0
local updateFrameDistance = 1
local dodgeDirection = 1
local dodgerEnabled = {}
--dodgerEnabled[unitID] == nil   --means unitID is not in dodgerIDs
--dodgerEnabled[unitID] == true  --means unitID is in dodgerIDs and enabled
--dodgerEnabled[unitID] == false --means unitID is in dodgerIDs and disabled due to manual cmd or death
local transports = {}
--transports[AunitID][BunitID] == true    --means AunitID transports BunitID and transportedBy[BunitID] == AunitID
local transportedBy = {}
--transportedBy[AunitID][BunitID] == true --means AunitID is transported by BunitID and transports[BunitID] == AunitID
--transports[AunitID][BunitID] == transportedBy[BunitID][AunitID]

local function isIdle(unitID)
    local givenCmds = spGetUnitCommands(unitID, 0)
    return (givenCmds and givenCmds == 0)
end

local function isNotTransported(unitID)
    local transporters = transportedBy[unitID]
    return not (transporters and next(transporters))
end

function widget:Initialize()
    local loadText  = "LOADED "
    if(Spring.IsReplay() or Spring.GetSpectatingState()) then
        Spring.Echo(loadText.."AND REMOVED "..widget:GetInfo().name)
        widgetHandler:RemoveWidget()
    else
        Spring.Echo(loadText..widget:GetInfo().name)
        local myUnits = Spring.GetTeamUnits(myTeamID)
        if myUnits then
            for myUnitIndx=1,#myUnits do
                local myUnitID = myUnits[myUnitIndx]
                if spValidUnitID(myUnitID) then
                    local myUnitDefID = spGetUnitDefID(myUnitID)
                    if isIdle(myUnitID) and select(5,spGetUnitHealth(myUnitID))==1 then
                        widget:UnitIdle(myUnitID, myUnitDefID, myTeamID)
                    end
                    if myUnitDefID == CharonID or myUnitDefID == HerculesID then
                        transports[myUnitID] = {}
                    end
                end
            end
            Spring.Echo(dodgerNr.." dodgers have been indexed")
        end
    end
end

function widget:PlayerChanged(playerID)
    myTeamID = Spring.GetMyTeamID()
end

function widget:TeamChanged(teamID)
    myTeamID = Spring.GetMyTeamID()
end

function widget:UnitIdle(unitID, unitDefID, unitTeam)
    if unitTeam==myTeamID then
    if dodgerEnabled[unitID] == nil then
        local unitDef = UnitDefs[unitDefID]
        if unitDef.speed~=0 and unitDef.canMove and not unitDef.isBuilder and maybeDodgers[unitDefID] then
            dodgerNr = dodgerNr + 1
            dodgerIDs[dodgerNr] = unitID
            local x, y, z = spGetUnitPosition(unitID,true,true)
            dodgerPositions[unitID] = {x, y, z}
            dodgerEnabled[unitID] = isNotTransported(unitID)
        end
    elseif dodgerEnabled[unitID] == false then
            local x, y, z = spGetUnitPosition(unitID,true,true)
            dodgerPositions[unitID] = {x, y, z}
            dodgerEnabled[unitID] = isNotTransported(unitID)
    end
    end
end

function widget:GameFrame(frame)
    if dodgerNr ~= 0 then
        if updateCounter == 0 then
            local dodgeDist = math.random(64,128)
            for idx=1,dodgerNr do
                local unitID = dodgerIDs[idx]
                local unitStates = unitID and spGetUnitStates(unitID)
                -- 74145: changed to only dodge with selected units, less sus..
                if unitStates and dodgerEnabled[unitID] and not unitStates.cloak and spIsUnitSelected(unitID) and isNotTransported(unitID) then
                    local nearestEnemy = spGetUnitNearestEnemy(unitID)
                    local pos = dodgerPositions[unitID]
                    local xToTarget, zToTarget
                    if nearestEnemy then
                        xToTarget,_,zToTarget = spGetUnitPosition(nearestEnemy)
                        xToTarget,zToTarget = xToTarget - pos[1], zToTarget - pos[3]
                        local normFactor = 1/math.sqrt(xToTarget*xToTarget + zToTarget*zToTarget)
                        xToTarget,zToTarget = normFactor*xToTarget, normFactor*zToTarget
                        xToTarget, zToTarget = pos[1] + dodgeDirection*dodgeDist*zToTarget, pos[3] - dodgeDirection*dodgeDist*xToTarget
                    else
                        xToTarget, zToTarget = pos[1] + dodgeDirection*dodgeDist, pos[3]
                    end
                    spGiveOrderToUnit(unitID, CMD_RAW_MOVE,{xToTarget, pos[2], zToTarget},{"alt"})
                end
            end
            updateFrameDistance = math.ceil(dodgeDist*0.8)
            dodgeDirection = - dodgeDirection
        end
        updateCounter = (updateCounter + 1)%updateFrameDistance
    end
end

function widget:UnitCommand(unitID, unitDefID, unitTeam, cmdID, cmdParams, cmdOptions)
    if not (cmdOptions.alt and cmdID == CMD_RAW_MOVE) then
        if dodgerEnabled[unitID] then
            dodgerEnabled[unitID] = false
            dodgerPositions[unitID] = nil
        end
        if transports[unitID] then
            if not cmdOptions.shift then
                for transportedID,_ in pairs(transports[unitID]) do
                    transportedBy[transportedID][unitID] = nil
                end
            transports[unitID] = {}
            end
            if cmdID == CMD_LOAD_UNITS then
                local transportTarget = cmdParams[1]
                local transportTargetDefID = transportTarget and spGetUnitDefID(transportTarget)
                local transportTargetDef = transportTargetDefID and UnitDefs[transportTargetDefID]
                if transportTargetDef and not transportTargetDef.cantBeTransported and (transportTargetDef.metalCost <= 1000 or unitDefID == HerculesID) then
                    if transportedBy[transportTarget] then
                        transportedBy[transportTarget][unitID] = true
                    else
                        transportedBy[transportTarget] = {[unitID] = true}
                    end
                    if transports[unitID] then
                        transports[unitID][transportTarget] = true
                    else
                        transports[unitID] = {[transportTarget] = true}
                    end
                    Spring.Echo(unitID.." loads "..transportTarget)
                end
            end
        end
    end
end

function widget:UnitDestroyed(unitID, unitDefID, unitTeam, attackerID, attackerDefID, attackerTeam)
    if dodgerEnabled[unitID] ~= nil then
        dodgerPositions[unitID] = nil
        dodgerEnabled[unitID] = false
    end
    if transportedBy[unitID] then
        for transporterID,_ in pairs(transportedBy[unitID]) do
            transports[transporterID][unitID] = nil
        end
        transportedBy[unitID] = nil
    elseif transports[unitID] then
        for transportedID,_ in pairs(transports[unitID]) do
            transportedBy[transportedID][unitID] = nil
            if not next(transportedBy[transportedID]) then
                transportedBy[transportedID] = nil
            end
        end
        transports[unitID] = nil
    end
end

function widget:UnitGiven(unitID, unitDefID, newTeam, oldTeam)
    if newTeam == myTeamID then
        if isIdle(unitID) then
            widget:UnitIdle(unitID, unitDefID, myTeamID)
        end
    else
        widget:UnitDestroyed(unitID, unitDefID, oldTeam, nil, nil, nil)
    end
end

function widget:UnitReverseBuilt(unitID, unitDefID, unitTeam)
    widget:UnitDestroyed(unitID, unitDefID, unitTeam, nil, nil, nil)
end

function widget:UnitCreated(unitID, unitDefID, unitTeam, builderID)
    if unitTeam == myTeamID and (unitDefID == CharonID or unitDefID == HerculesID) then
        transports[unitID] = {}
    end
end
