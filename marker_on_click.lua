function widget:GetInfo()
    return {
        name      = "Marker On Click",
        desc      = "Place marker on single click",
        author    = "Helwor",
        date      = "October 2022", 
        license   = "GNU GPL v2",
        layer     = 0,
        enabled   = true,
    }
end
-- Config
local MOVE_THRESHOLD = 5 -- can place a marker until the mouse moved this many pixel from the click position
local DOUBLECLICK_TIME = 0.28 -- how long shall we wait for a double click to happen before authorizing to place a marker
                              -- , value try to be as close as possible of the text label trigger
--
-- NOTE: since we use Update CallIn, the delay may vary depending on lag
-- the widget is disabled at start of game if no hotkey is found for drawing on map

local spMarkerAddPoint = Spring.MarkerAddPoint
local spTraceScreenRay = Spring.TraceScreenRay
local spGetMouseState = Spring.GetMouseState
local Echo = Spring.Echo

local hotkey = {}

local catchDrawing, dontCatchThisOne, drawing, _, pos
local doubleClickDelay, waitDblClick, waitRelease

function widget:KeyPress(key,mods,isRepeat)
    if isRepeat then
        return
    end
    catchDrawing = hotkey[key]
end
function widget:KeyRelease(key)
    if catchDrawing then
        catchDrawing = not hotkey[key]
    end
end
function widget:Update(dt)
    if not catchDrawing then
        return
    end
    if doubleClickDelay then
        doubleClickDelay = doubleClickDelay - dt
        if doubleClickDelay<=0 then
            doubleClickDelay = false
        end
    end
    local mx,my,lmb = spGetMouseState()
    if waitRelease then
        if lmb then 
            return
        end
        waitRelease = false
    end
    if waitDblClick then
        if lmb then -- text label should have been triggered there
            catchDrawing = false
            waitDblClick = false
            doubleClickDelay = false
        else
            waitDblClick = doubleClickDelay
            if not waitDblClick then
                spMarkerAddPoint(pos[1],pos[2],pos[3])
            end
        end
        return
    end

    if not drawing then
        if lmb then
            last_mx,last_my = mx,my
            drawing = true
            doubleClickDelay = DOUBLECLICK_TIME
            dontCatchThisOne = false
        end
    else
        if not lmb then
            drawing = false
        end
        if dontCatchThisOne then
            return
        end
        local delta = ((last_mx-mx)^2 + (last_my-my)^2)^0.5
        if delta>MOVE_THRESHOLD then
            dontCatchThisOne = true
            return
        end
        if not (drawing or dontCatchThisOne) then
            -- we're half way there, get the pos for our marker
            _,pos = spTraceScreenRay(mx, my, true, true, false,false)
            if not pos then
                return
            end
            waitDblClick = true
            waitRelease = true
        end
    end
end

function widget:Initialize()
    for _,key in pairs(WG.crude.GetHotkeys("drawinmap") or {}) do
        hotkey[key:byte(-1)] = true
    end
    if not next(hotkey) then
        Echo(widget:GetInfo().name .. ' disabled, no hotkey for drawing on map')
        widgetHandler:RemoveWidget(widget)
    end
end