-- Copyright (C) 2022 Brackman
function widget:GetInfo()
	return {
		name         = "Smartest Builders",
		desc         = "Idle Builders repair/build/reclaim depending on current ressources",
		author       = "Brackman",
		date         = "2022-01-16",
		license      = "GNU GPL v2",
		--layer        = 0,
		enabled      = true
	}
end

include("keysym.h.lua")
VFS.Include("LuaRules/Configs/customcmds.h.lua")
local mmax = math.max

local spGetUnitRulesParam = Spring.GetUnitRulesParam
local spGetUnitsInCylinder = Spring.GetUnitsInCylinder
local spGetFeaturesInCylinder = Spring.GetFeaturesInCylinder
local spGetFeatureResources = Spring.GetFeatureResources
local spGetUnitPosition = Spring.GetUnitPosition
local spGetUnitNearestEnemy = Spring.GetUnitNearestEnemy
local spGetUnitStates = Spring.GetUnitStates
local spGetUnitHealth = Spring.GetUnitHealth
local spGetUnitAllyTeam = Spring.GetUnitAllyTeam
local myTeamID = Spring.GetMyTeamID()
local myAllyTeamID = Spring.GetMyAllyTeamID()
local spGetGroundHeight = Spring.GetGroundHeight
local spGetUnitResources = Spring.GetUnitResources
local spGetUnitCommands = Spring.GetUnitCommands
local spGetGameFrame = Spring.GetGameFrame
local spIsUnitSelected = Spring.IsUnitSelected
local spGetUnitTeam = Spring.GetUnitTeam
local spValidUnitID = Spring.ValidUnitID
local spGetUnitDefID = Spring.GetUnitDefID
local spGetTeamResources = Spring.GetTeamResources
local spGetUnitIsCloaked = Spring.GetUnitIsCloaked
local spGetTeamList = Spring.GetTeamList
local spEcho = Spring.Echo
local spGiveOrderToUnit = Spring.GiveOrderToUnit

local idleBuilderList = {}
local idleBuilderListLength = 0
local idleBuilderListNotEmpty = false
local idleBuilderNr = 0 --number of all in idleBuilderList for which idleBuilderEnabledList is true
local currentListIndex = 1
local idleBuilderStatusList = {}
  --nil means builder is dead or has never been idle
  --function reference means builder is currently doing that function (if idleBuilderEnabledList[]==true); builders will always have reclaim function references instead of ReclaimOnlyIfNoTeamExcess function references so that team excess is updated correctly; builders will have updateBuilder function if update is needed even if they are busy
local idleBuilderParamsList = {}
local idleBuilderRepairTargetList = {}
local idleBuilderReclaimTargetList = {}
local idleBuilderEnabledList = {}
  --nil means builder is not in idleBuilderList
  --false means builder is disabled due to a manual command or death but is still in idleBuilderList
  --true means builder is enabled and in idleBuilderList
local idleBuilderLastWidgetCmdFrameList = {}
local idleBuilderLastWidgetCmdParamsList = {}
local updateFrameDistance = 30
local idleBuilderNeedsActionReset = {}
local currentlyUpdatedBuilderID = nil --this is only set by GameFrame to lock the updating for UnitIdle
local buildRadiusExtension = 201
local lastComID = nil

--only checks personal and area cloaking state to enable/disable builder:
local idleCloakerList = {} --list of enabled cloaked areaCloakable builders
local idleCloakerListLength = 0
local idleCloakerListNotEmpty = false
local idleCloakerNr = 0 --number of all in idleCloakerList for which idleCloakerEnabledList is true
local idleCloakerEnabledList = {}
  --nil means cloaker is not in idleCloakerList
  --false means cloaker is disabled due to a manual command or death or decloaking but is still in idleCloakerList
  --true means cloaker is enabled and in idleCloakerList
local orderTable = {{}, {}, {}}

local caretakerDefID = UnitDefNames.staticcon.id
local striderhubDefID = UnitDefNames.striderhub.id
local athenaDefID = UnitDefNames.athena.id
local funnelwebDefID = UnitDefNames.striderfunnelweb.id
local terraDefID = UnitDefNames.terraunit.id

local fireflyDefID = UnitDefNames.dronelight.id
local clawDefID = UnitDefNames.wolverine_mine.id
local lampDefID = UnitDefNames.tele_beacon.id
local gullDefID = UnitDefNames.dronecarry.id
local glintDefID = UnitDefNames.starlight_satellite.id
local viperDefID = UnitDefNames.droneheavyslow.id

local CMD_REPAIR = CMD.REPAIR
local CMD_RECLAIM = CMD.RECLAIM
local CMD_OPT_CTRL = CMD.OPT_CTRL
local CMD_FIGHT = CMD.FIGHT
local CMD_STOP = CMD.STOP

local MIN_STORAGE = 0.5
local HIDDEN_STORAGE = 10000

local function nrToString(nr)
	if nr == nil then
		return "nil"
	end
	return nr
end

local function boolToNr(bool)
	if bool then
		if bool == true then
			return 1
		else
			return bool
		end
	else
		return 0
	end
end

local function unequalArrays(a, b)
	if type(b) ~= "table" then
		spEcho("WARNING! cmdParams IS NOT A TABLE")
		return true
	elseif #a == #b then
		local unequal = false
		for arrayIdx = 1, #a do
			unequal = (unequal or a[arrayIdx] ~= b[arrayIdx])
		end
		return unequal
	end
	return true
end

local function isBuilderType(unitDefID)
	local unitDef = UnitDefs[unitDefID]
	return (unitDef.isBuilder and not unitDef.isFactory)
end

local function isCom(unitDefID)
	local unitDef = UnitDefs[unitDefID]
	return (unitDef.isBuilder and unitDef.metalStorage > 0)
end

local function isAreaCloaker(unitID, unitDefID)
	local areaCloakable = UnitDefs[unitDefID].customParams.area_cloak
	local comAreaCloakable = spGetUnitRulesParam(unitID, "comm_area_cloak")
	return ((areaCloakable and areaCloakable ~=0) or (comAreaCloakable and comAreaCloakable ~=0))
end

local function isAreaCloaked(unitID)
	local areaCloaked = spGetUnitRulesParam(unitID, "areacloaked")
	return (areaCloaked and areaCloaked ~=0)
end

local function isInvalidUnit(unitDefID)
	return (unitDefID == fireflyDefID or unitDefID == clawDefID or unitDefID == lampDefID or unitDefID == gullDefID or unitDefID == glintDefID or unitDefID == viperDefID or not unitDefID)
end

local function isIdle(unitID)
	local givenCmds = spGetUnitCommands(unitID, 0)
	return (givenCmds and givenCmds == 0)
end

local function resStatus(teamID, resType, bp, rateChange) --1: deficit, 2: normal, 3: excess
	local eCurrMy, eStorMy,_, eIncoMy, eExpeMy,_,_,_ = spGetTeamResources(teamID, resType)
	eStorMy = mmax(eStorMy - HIDDEN_STORAGE, MIN_STORAGE)
	local totalRate = eIncoMy - eExpeMy + rateChange
	if eStorMy > MIN_STORAGE then
		if eCurrMy > eStorMy then
			eCurrMy = eStorMy
		end
		if eCurrMy < 100 then
			if totalRate >= bp then
				if eCurrMy >= eStorMy-50 then
					return 3
				else
					return 2
				end
			else
				return 1
			end
		else
			if totalRate < 0 then
				return 2
			else
				if eCurrMy >= eStorMy-50 then
					return 3
				else
					return 2
				end
			end
		end
	else
		if totalRate < bp*0.5 then
			return 1
		elseif totalRate > bp then
			return 3
		else
			return 2
		end
	end
end

local function wait(unitID, x, z, range, mobile, bp, busy, rateChange)
	if mobile then
		idleBuilderLastWidgetCmdParamsList[unitID] = {x,spGetGroundHeight(x,z),z}
		spGiveOrderToUnit(unitID, CMD_RAW_MOVE, idleBuilderLastWidgetCmdParamsList[unitID],{})
	else
		idleBuilderLastWidgetCmdParamsList[unitID] = {}
		spGiveOrderToUnit(unitID, CMD_STOP, idleBuilderLastWidgetCmdParamsList[unitID],{})
	end
	idleBuilderStatusList[unitID] = wait
	return false --noActionFound
end

local function tryReclaimEnemy(unitID, x, z, range, mobile)
	if mobile then
		range = range - buildRadiusExtension
	end
	local unitsInRange = spGetUnitsInCylinder(x,z,range)
	local maxBuildProgress = 1.0
	local minCaptureProgress = 0.5
	local unitToReclaim = nil
	for indx=1,#unitsInRange do
		local unitInRange = unitsInRange[indx]
	if spGetUnitAllyTeam(unitInRange) ~= myAllyTeamID then
			local _,_,_,captureProgress,buildProgress = spGetUnitHealth(unitInRange)
			if buildProgress and buildProgress < 1.0 and unitInRange ~= unitID and captureProgress and captureProgress <= minCaptureProgress and not isInvalidUnit(spGetUnitDefID(unitInRange)) then
				if (buildProgress > maxBuildProgress and captureProgress <= minCaptureProgress) or captureProgress < minCaptureProgress then
					maxBuildProgress = buildProgress
					minCaptureProgress = captureProgress
					unitToReclaim = unitInRange
				end
			end
		end
	end
	if unitToReclaim and spValidUnitID(unitID) then
		idleBuilderReclaimTargetList[unitID] = unitToReclaim
		idleBuilderLastWidgetCmdParamsList[unitID] = {unitToReclaim}
		spGiveOrderToUnit(unitID, CMD_RECLAIM, idleBuilderLastWidgetCmdParamsList[unitID], {})
		idleBuilderStatusList[unitID] = updateBuilder--instead of tryReclaimEnemy to update checks for best target
		return false
	else
		return true --noActionFound
	end
end

local function tryRepair(unitID, x, z, range, mobile, bp, busy, rateChange)
	local unitsInRange = spGetUnitsInCylinder(x,z,range)
	local minRelHealth = 1.0
	local minCaptureProgress = 0.5
	local unitToRepair = nil
	for indx=1,#unitsInRange do
		local unitInRange = unitsInRange[indx]
	if spGetUnitAllyTeam(unitInRange) == myAllyTeamID then
			local health,maxHealth,_,captureProgress,buildProgress = spGetUnitHealth(unitInRange)
			if health and maxHealth and health < maxHealth and unitInRange ~= unitID and buildProgress and buildProgress >= 1.0 and captureProgress and captureProgress <= minCaptureProgress and not isInvalidUnit(spGetUnitDefID(unitInRange)) and maxHealth ~= 0 then
				local relHealth = health/maxHealth
				if (relHealth < minRelHealth and captureProgress <= minCaptureProgress) or captureProgress < minCaptureProgress then
					minRelHealth = relHealth
					minCaptureProgress = captureProgress
					unitToRepair = unitInRange
				end
			end
		end
	end
	if unitToRepair and spValidUnitID(unitID) then
		idleBuilderRepairTargetList[unitID] = unitToRepair
		idleBuilderLastWidgetCmdParamsList[unitID] = {unitToRepair}
		spGiveOrderToUnit(unitID, CMD_REPAIR, idleBuilderLastWidgetCmdParamsList[unitID], {})
		idleBuilderStatusList[unitID] = tryRepair
		return false
	else
		return true
	end
end

local function tryRepairAndKeepTarget(unitID, x, z, range, mobile, bp, busy, rateChange)
	if busy and idleBuilderStatusList[unitID] == tryRepair and spValidUnitID(idleBuilderRepairTargetList[unitID]) then
		local xTarget,_,zTarget = spGetUnitPosition(idleBuilderRepairTargetList[unitID])
		if xTarget then
			xTarget,zTarget = xTarget-x,zTarget-z
			if xTarget*xTarget + zTarget*zTarget <= range*range then
				local health,maxHealth,_,captureProgress,buildProgress = spGetUnitHealth(idleBuilderRepairTargetList[unitID])
				if health < maxHealth and captureProgress <= 0.5 and buildProgress >= 1.0 then
					return false
				end
			end
		end
	end
	return tryRepair(unitID, x, z, range, mobile, bp, busy, rateChange)
end

local function tryBuildOwn(unitID, x, z, range, mobile, bp, busy, rateChange) --only own squad so far
	local unitsInRange = spGetUnitsInCylinder(x,z,range,myTeamID)
	local maxBuildProgress = 0
	local minCaptureProgress = 0.5
	local bestMobility = 1
	local unitToRepair = nil
	for indx=1,#unitsInRange do
		local unitInRange = unitsInRange[indx]
		local _,_,_,captureProgress,buildProgress = spGetUnitHealth(unitInRange)
		if buildProgress and buildProgress < 1.0 and unitInRange ~= unitID and captureProgress and captureProgress <= minCaptureProgress then
			local unitDefInRange = spGetUnitDefID(unitInRange)
			local mobility
			if unitDefInRange and UnitDefs[unitDefInRange].speed == 0 then
				mobility = 0
			else
				mobility = 1
			end
			if ((mobility < bestMobility or (mobility <= bestMobility and buildProgress > maxBuildProgress) and captureProgress <= minCaptureProgress) or captureProgress < minCaptureProgress) and not isInvalidUnit(unitDefInRange) then
				minCaptureProgress = captureProgress
				bestMobility = mobility
				maxBuildProgress = buildProgress
				unitToRepair = unitInRange
			end
		end
	end
	if unitToRepair and spValidUnitID(unitID) then
		idleBuilderLastWidgetCmdParamsList[unitID] = {unitToRepair}
		spGiveOrderToUnit(unitID, CMD_REPAIR, idleBuilderLastWidgetCmdParamsList[unitID], {})
		idleBuilderStatusList[unitID] = updateBuilder--instead of tryBuildOwn to update checks for best target
		return false
	else
		return true
	end
end

local function tryReclaimMetal(unitID, x, z, range, mobile, bp, busy, rateChange)
	featuresInRange = spGetFeaturesInCylinder(x, z, range)
	local noMetalFound = true
	local indx = 1
	while noMetalFound and indx <= #featuresInRange do
		noMetalFound = (spGetFeatureResources(featuresInRange[indx]) < 0.05)
		indx = indx + 1
	end
	if (not noMetalFound) and spValidUnitID(unitID) then
		idleBuilderLastWidgetCmdParamsList[unitID] = {x, spGetGroundHeight(x,z), z, range}
		spGiveOrderToUnit(unitID, CMD_RECLAIM, idleBuilderLastWidgetCmdParamsList[unitID], {})
		idleBuilderStatusList[unitID] = tryReclaimMetal
		return false
	else
		return true
	end
end

local function tryReclaimEnergy(unitID, x, z, range, mobile, bp, busy, rateChange)
	featuresInRange = spGetFeaturesInCylinder(x, z, range)
	local noEnergyFound = true
	local indx = 1
	while noEnergyFound and indx <= #featuresInRange do
		noEnergyFound = (select(3,spGetFeatureResources(featuresInRange[indx])) < 0.05)
		indx = indx + 1
	end
	if (not noEnergyFound) and spValidUnitID(unitID) then
		idleBuilderLastWidgetCmdParamsList[unitID] = {x, spGetGroundHeight(x,z), z, range}
		spGiveOrderToUnit(unitID, CMD_RECLAIM, idleBuilderLastWidgetCmdParamsList[unitID], CMD_OPT_CTRL)
		idleBuilderStatusList[unitID] = tryReclaimEnergy
		return false
	else
		return true
	end
end

local function tryReclaimMetalOnlyIfNoTeamExcess(unitID, x, z, range, mobile, bp, busy, rateChange)
	local noTeamMetalExcess = false
	local allyTeamTeams = spGetTeamList(myAllyTeamID)
	if allyTeamTeams and next(allyTeamTeams) then
		for teamIndex,teamID in pairs(allyTeamTeams) do
			noTeamMetalExcess = noTeamMetalExcess or (teamID ~= myTeamID and resStatus(teamID, "metal", bp, rateChange.metal) < 3)
		end
	end
	if noTeamMetalExcess then
		return tryReclaimMetal(unitID, x, z, range, mobile, bp, busy, rateChange)
	end
	return true --noActionFound
end

local function tryReclaimEnergyOnlyIfNoTeamExcess(unitID, x, z, range, mobile, bp, busy, rateChange)
	local noTeamEnergyExcess = false
	local allyTeamTeams = spGetTeamList(myAllyTeamID)
	if allyTeamTeams and next(allyTeamTeams) then
		for teamIndex,teamID in pairs(allyTeamTeams) do
			noTeamEnergyExcess = noTeamEnergyExcess or (teamID ~= myTeamID and resStatus(teamID, "energy", bp, rateChange.energy) < 3)
		end
	end
	if noTeamEnergyExcess then
		return tryReclaimEnergy(unitID, x, z, range, mobile, bp, busy, rateChange)
	end
	return true --noActionFound
end

local function tryBuildNoobAlly(unitID, x, z, range, mobile, bp, busy, rateChange)
	--idleBuilderStatusList[unitID] = tryBuildNoobAlly
	return true --noActionFound
end

local function updateBuilder(unitID, busy)
	if not busy then
		idleBuilderNeedsActionReset[unitID] = nil
	end
	local idleBuilderParams = idleBuilderParamsList[unitID]
	if not idleBuilderParams then
		updateBuilderParams(unitID, spGetUnitDefID(unitID) or caretakerDefID)--just to be safe
		return nil
	end
	local x = idleBuilderParams.x
	local z = idleBuilderParams.z
	local range = idleBuilderParams.range
	local mobile = idleBuilderParams.mobile
	local bp = idleBuilderParams.bp
	if spValidUnitID(unitID) and tryReclaimEnemy(unitID, x, z, range, mobile) then --and (idleBuilderStatusList[unitID] ~= tryReclaimEnemy or not busy)
		local noActionFound = true
		local orderTableIdx = 1
		local status = {}
		local rateChange
		if busy and spValidUnitID(unitID) then
			local metalMake, metalUse, energyMake, energyUse = spGetUnitResources(unitID)
			rateChange = {metal = (metalUse or 0) - (metalMake or 0), energy = (energyUse or 0) - (energyMake or 0)}
		else
			rateChange = {metal = 0, energy = 0}
		end
		for _,resType in pairs({"metal", "energy"}) do
			status[resType] = resStatus(myTeamID, resType, bp, rateChange[resType])
		end
		if busy then
			while noActionFound and orderTableIdx <= #(orderTable[status.metal][status.energy]) do
				noActionFound = (idleBuilderStatusList[unitID] ~= orderTable[status.metal][status.energy][orderTableIdx])
				if noActionFound then
					noActionFound = orderTable[status.metal][status.energy][orderTableIdx](unitID, x, z, range, mobile, bp, busy, rateChange)
					orderTableIdx = orderTableIdx + 1
				end
			end
			if noActionFound and idleBuilderStatusList[unitID] ~= wait and spValidUnitID(unitID) then
				wait(unitID, x, z, range, mobile, bp, busy, rateChange)
			end
		else
			while noActionFound and orderTableIdx <= #(orderTable[status.metal][status.energy]) do
				noActionFound = orderTable[status.metal][status.energy][orderTableIdx](unitID, x, z, range, mobile, bp, busy, rateChange)
				orderTableIdx = orderTableIdx + 1
			end
			if noActionFound and spValidUnitID(unitID) then
				wait(unitID, x, z, range, mobile, bp, busy, rateChange)
				--idleBuilderStatusList[unitID] = wait
			end
		end
	end
	idleBuilderLastWidgetCmdFrameList[unitID] = spGetGameFrame()
	return nil
end

local function updateBuilderParams(unitID, unitDefID)
	local unitDef = UnitDefs[unitDefID]
	local x,_,z = spGetUnitPosition(unitID)
	local range, mobile
	if unitDef.speed == 0 then
		range = unitDef.buildDistance
		mobile = false
	else
		range = unitDef.buildDistance + buildRadiusExtension
		mobile = true
	end
	idleBuilderParamsList[unitID] = {x = x, z = z, range = range, mobile = mobile, bp = unitDef.buildSpeed}
	return unitID
end

local function disableBuilder(unitID)
	if idleBuilderEnabledList[unitID] then
		idleBuilderNr = idleBuilderNr - 1
		idleBuilderListNotEmpty = (idleBuilderNr > 0)
		idleBuilderEnabledList[unitID] = false
		spEcho("Builder disabled, idleBuilderListLength = "..idleBuilderListLength..", idleBuilderNr = "..idleBuilderNr)
	end
end

local function addBuilder(unitID, unitDefID)
	if idleBuilderEnabledList[unitID] == nil then
		if unitDefID then
			updateBuilderParams(unitID, unitDefID)
			if isCom(unitDefID) then
				lastComID = unitID
			end
		end
		idleBuilderList[idleBuilderListLength + 1] = unitID
		idleBuilderListLength = idleBuilderListLength + 1
		idleBuilderNr = idleBuilderNr + 1
		if unitDefID then
			spEcho("Builder added, idleBuilderListLength = "..idleBuilderListLength..", idleBuilderNr = "..idleBuilderNr..", bp = "..nrToString(idleBuilderParamsList[unitID].bp))
		end
	elseif not idleBuilderEnabledList[unitID] then
		idleBuilderNr = idleBuilderNr + 1
		local idleBuilderParams = idleBuilderParamsList[unitID]
		if not idleBuilderParams then
			unitID = updateBuilderParams(unitID, unitDefID or caretakerDefID)--just to be safe
		end
		idleBuilderParams.x,_,idleBuilderParams.z = spGetUnitPosition(unitID)
		if unitDefID and isCom(unitDefID) then
			lastComID = unitID
		end
		spEcho("Builder reenabled, idleBuilderListLength = "..idleBuilderListLength..", idleBuilderNr = "..idleBuilderNr)
	end
	if idleBuilderStatusList[unitID] ~= wait or not idleBuilderEnabledList[unitID] then
		if unitID ~= currentlyUpdatedBuilderID and spGetGameFrame() > boolToNr(idleBuilderLastWidgetCmdFrameList[unitID]) then--
			updateBuilder(unitID, false)
		else
			idleBuilderNeedsActionReset[unitID] = true
		end
		idleBuilderEnabledList[unitID] = true
		idleBuilderListNotEmpty = true
	end
end

local function disableCloaker(unitID)
	if idleCloakerEnabledList[unitID] then
		idleCloakerNr = idleCloakerNr - 1
		idleCloakerListNotEmpty = (idleCloakerNr > 0)
		idleCloakerEnabledList[unitID] = false
		spEcho("Cloaker disabled")
	end
end

local function addCloaker(unitID)
	if idleCloakerEnabledList[unitID] == nil then
		idleCloakerList[idleCloakerListLength + 1] = unitID
		idleCloakerListLength = idleCloakerListLength + 1
		idleCloakerNr = idleCloakerNr + 1
		spEcho("Cloaker added")
	elseif not idleCloakerEnabledList[unitID] then
		idleCloakerNr = idleCloakerNr + 1
		spEcho("Cloaker reenabled")
	end
	idleCloakerEnabledList[unitID] = true
	idleCloakerListNotEmpty = true
end

function widget:Initialize()
	local loadText  = "LOADED "
	if(Spring.IsReplay() or Spring.GetSpectatingState()) then
		spEcho(loadText.."AND REMOVED "..widget:GetInfo().name)
		widgetHandler:RemoveWidget()
	else
		spEcho(loadText..widget:GetInfo().name)
		--metal deficit:
		orderTable[1][1] = {tryReclaimMetal, tryReclaimEnergy, tryRepairAndKeepTarget, tryBuildOwn} --energy deficit
		orderTable[1][2] = {tryRepairAndKeepTarget, tryReclaimMetal, tryReclaimEnergy, tryBuildOwn} --energy normal
		orderTable[1][3] = {tryRepairAndKeepTarget, tryReclaimMetal, tryBuildOwn, tryReclaimEnergyOnlyIfNoTeamExcess} --energy excess
		--metal normal:
		orderTable[2][1] = {tryReclaimEnergy, tryReclaimMetal, tryBuildOwn, tryRepairAndKeepTarget} --energy deficit
		orderTable[2][2] = {tryRepairAndKeepTarget, tryBuildOwn, tryReclaimMetal, tryReclaimEnergy} --energy normal
		orderTable[2][3] = {tryRepairAndKeepTarget, tryBuildOwn, tryReclaimMetal, tryReclaimEnergyOnlyIfNoTeamExcess} --energy excess
		--metal excess:
		orderTable[3][1] = {tryReclaimEnergy, tryBuildOwn, tryRepairAndKeepTarget, tryReclaimMetalOnlyIfNoTeamExcess} --energy deficit
		orderTable[3][2] = {tryBuildOwn, tryRepairAndKeepTarget, tryReclaimEnergy, tryReclaimMetalOnlyIfNoTeamExcess, tryBuildNoobAlly} --energy normal
		orderTable[3][3] = {tryBuildOwn, tryRepairAndKeepTarget, tryReclaimMetalOnlyIfNoTeamExcess, tryReclaimEnergyOnlyIfNoTeamExcess, tryBuildNoobAlly} --energy excess
		local myUnits = Spring.GetTeamUnits(myTeamID)
		if myUnits then
			for myUnitIndx=1,#myUnits do
				local myUnitID = myUnits[myUnitIndx]
				if spValidUnitID(myUnitID) and isIdle(myUnitID) and select(5,spGetUnitHealth(myUnitID))==1 then
					widget:UnitIdle(myUnitID, spGetUnitDefID(myUnitID), myTeamID)
				end
			end
			spEcho(idleBuilderListLength.." builders have been indexed")----
		end
	end
end

function widget:UnitIdle(unitID, unitDefID, unitTeam)
	if unitTeam==myTeamID and isBuilderType(unitDefID) then --and not spIsUnitSelected(unitID)
		local areaCloakable = isAreaCloaker(unitID, unitDefID)
		if not(areaCloakable and isAreaCloaked(unitID)) then
			addBuilder(unitID, unitDefID)
		end
		if areaCloakable and spGetUnitIsCloaked(unitID) then
			addCloaker(unitID)
		end
	end
end

function widget:UnitFinished(unitID, unitDefID, unitTeam)
	if idleBuilderEnabledList[unitID] and (unitDefID == caretakerDefID or unitDefID == striderhubDefID or unitDefID == athenaDefID or unitDefID == funnelwebDefID) then
		addBuilder(unitID, unitDefID)
	end
end

function widget:UnitDestroyed(unitID, unitDefID, unitTeam, attackerID, attackerDefID, attackerTeam)
	if(unitTeam == myTeamID and idleBuilderEnabledList[unitID] ~= nil) then --and select(5,spGetUnitHealth(unitID))==1
		disableBuilder(unitID)
		idleBuilderStatusList[unitID] = nil
		idleBuilderLastWidgetCmdParamsList[unitID] = nil
		spEcho("Builder deleted, idleBuilderListLength = "..idleBuilderListLength..", idleBuilderNr = "..idleBuilderNr)
	end
	disableCloaker(unitID)
end

function widget:UnitGiven(unitID, unitDefID, newTeam, oldTeam)
	if newTeam == myTeamID then
		if isIdle(unitID) then
			widget:UnitIdle(unitID, unitDefID, unitTeam)
		end
	else
		widget:UnitDestroyed(unitID, unitDefID, oldTeam, nil, nil, nil)
	end
end

function widget:UnitReverseBuilt(unitID, unitDefID, unitTeam)
	if(unitTeam == myTeamID) then -- and idleBuilderEnabledList[unitID] ~= nil and select(5,spGetUnitHealth(unitID))==1
		disableBuilder(unitID)
		disableCloaker(unitID)
	end
end

function widget:PlayerChanged(playerID)
	myTeamID = Spring.GetMyTeamID()
end

function widget:TeamChanged(teamID)
	myTeamID = Spring.GetMyTeamID()
end

function widget:GameFrame(frame)
	if frame % updateFrameDistance == 0 then
		if idleCloakerListNotEmpty then
			for cloakerIdx=1,idleCloakerListLength do
				cloakerID = idleCloakerList[cloakerIdx]
				if idleCloakerEnabledList[cloakerID] then
					if isAreaCloaked(cloakerID) then
						disableBuilder(cloakerID)
					elseif not idleBuilderEnabledList[cloakerID] then
						addBuilder(cloakerID, spGetUnitDefID(cloakerID))
					end
				end
			end
		end
		if idleBuilderListNotEmpty then
			repeat currentListIndex = (currentListIndex % idleBuilderListLength) + 1
			until idleBuilderEnabledList[idleBuilderList[currentListIndex]]
			currentlyUpdatedBuilderID = idleBuilderList[currentListIndex]
			currentlyUpdatedBuilderID = updateBuilder(currentlyUpdatedBuilderID, not idleBuilderNeedsActionReset[currentlyUpdatedBuilderID])
		end
		WG.smartestBuildersFrame = frame
	end
end

function widget:UnitCommand(unitID, unitDefID, unitTeam, cmdID, cmdParams, cmdOptions)
	if unitTeam==myTeamID and spIsUnitSelected(unitID) and idleBuilderLastWidgetCmdParamsList[unitID] and unequalArrays(idleBuilderLastWidgetCmdParamsList[unitID], cmdParams) and not(cmdID == CMD_FIGHT and (unitDefID == caretakerDefID or unitDefID == striderhubDefID)) and cmdID ~= CMD_STOP and cmdID ~= 1 then --and idleBuilderLastWidgetCmdFrameList[unitID] and spGetGameFrame() > idleBuilderLastWidgetCmdFrameList[unitID]  then --and cmdOptions ~= CMD_OPT_ALT
		disableBuilder(unitID)
		disableCloaker(unitID)
	end
end

function widget:UnitCreated(unitID, unitDefID, unitTeam, builderID)
	--just in case a new builder has the same unitID as a previously destroyed one that has not been deleted properly (because readded):
	if idleBuilderEnabledList[unitID] ~= nil then
		widget:UnitDestroyed(unitID, unitDefID, oldTeam, nil, nil, nil)
		if unitDefID and isBuilderType(unitDefID) then
			updateBuilderParams(unitID, unitDefID)
		end
	elseif idleCloakerEnabledList[unitID] ~= nil then
		disableCloaker(unitID)
	end
	if unitTeam==myTeamID then
		if unitDefID == caretakerDefID or unitDefID == striderhubDefID or unitDefID == athenaDefID or unitDefID == funnelwebDefID then
			addBuilder(unitID, unitDefID)
		elseif isCom(unitDefID) then
			if builderID then
				widget:UnitIdle(unitID, unitDefID, unitTeam)
			elseif lastComID and idleBuilderEnabledList[lastComID] then
				widget:UnitIdle(unitID, unitDefID, unitTeam)
			end
		end
	end
	if idleBuilderListNotEmpty and unitDefID ~= terraDefID then --not updating on terra solves bug that terra build cmd cancels 1st building in queue
		local x,_,z = spGetUnitPosition(unitID)
		local unitsInRange = spGetUnitsInCylinder(x,z,500,myTeamID)
		for indx=1,#unitsInRange do
			local unitInRange = unitsInRange[indx]
			if idleBuilderEnabledList[unitInRange] and unitInRange ~= currentlyUpdatedBuilderID then
				updateBuilder(unitInRange, true)
			end
		end
	end
end

function widget:UnitCloaked(unitID, unitDefID, unitTeam)
	if unitTeam == myTeamID and isBuilderType(unitDefID) and isAreaCloaker(unitID, unitDefID) then --and idleBuilderEnabledList[unitID] ~= nil and spGetUnitIsCloaked(unitID)
		if isIdle(unitID) then --idleBuilderEnabledList[unitID] then
			addCloaker(unitID)
		end
		if isAreaCloaked(unitID) then
			disableBuilder(unitID)
		end
	end
end

function widget:UnitDecloaked(unitID, unitDefID, unitTeam)
	if unitTeam == myTeamID then
		if isAreaCloaker(unitID, unitDefID) and isBuilderType(unitDefID) and isIdle(unitID) then
			addBuilder(unitID, unitDefID)
		end
		disableCloaker(unitID)
	end
end